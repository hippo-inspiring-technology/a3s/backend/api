package main

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	appointments "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/handlers"
	users "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/handlers"
)

func main() {
	users := users.NewUsersHandler()
	appointments := appointments.NewAppointmentsHandler()
	router := httprouter.New()
	router.POST("/users/registration", users.Register)
	router.POST("/users/login", users.Login)
	router.POST("/users/profile/:userID", users.SaveUser)
	router.GET("/users/profile/:userID", users.GetUser)
	router.POST("/users/signin-with-apple", users.SignInWithApple)
	router.GET("/appointments/users/:userID", appointments.GetAppointmentsByUserID)

	log.Println("Server Starting at port 3000 ...")
	err := http.ListenAndServe(":3000", router)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
