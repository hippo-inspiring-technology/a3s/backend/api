module gitlab.com/hippo-inspiring-technology/a3s/backend/api

go 1.17

require (
	github.com/Timothylock/go-signin-with-apple v0.0.0-20210924040446-1aece0ab1da7
	github.com/aws/aws-sdk-go v1.34.5
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-jwt/jwt/v4 v4.2.0
	github.com/google/uuid v1.1.2
	github.com/julienschmidt/httprouter v1.3.0
	github.com/stretchr/testify v1.6.1
	github.com/tideland/golib v4.24.2+incompatible // indirect
	github.com/tideland/gorest v2.15.5+incompatible // indirect
	golang.org/x/crypto v0.0.0-20211202192323-5770296d904e
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/jmespath/go-jmespath v0.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
