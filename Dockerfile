FROM golang:1.17-alpine AS build

RUN apk --no-cache add ca-certificates

ENV APP /src/gitlab.com/hippo-inspiring-technology/a3s/backend/api
ENV WORKDIR ${GOPATH}${APP}

WORKDIR $WORKDIR/cmd/http
ADD . $WORKDIR

RUN CGO_ENABLED=0 go build -o /bin/api

FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /bin/api /bin/api
ENTRYPOINT ["/bin/api"]
