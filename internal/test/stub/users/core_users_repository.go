package stub

import (
	"errors"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/uuid"
)

type UsersRepository struct {
	Users         map[string]model.User
	ErrorInExist  bool
	ErrorInSave   bool
	ErrorToReturn error
}

func (s UsersRepository) Update(u model.User) error {
	if s.ErrorInSave == true {
		return errors.New("Error in storage")
	}

	s.Users[u.ID] = u

	return nil
}

func (s UsersRepository) Exist(ID string) (bool, error) {
	if s.ErrorInExist == true {
		return false, errors.New("Error in storage")
	}

	_, ok := s.Users[ID]

	return ok, nil
}

func (s UsersRepository) GetByID(ID string) (model.User, error) {
	user, ok := s.Users[ID]

	if !ok {
		return model.User{}, errors.New("User not exist")
	}

	return user, nil
}

func (s UsersRepository) GetByEmail(email string) (model.User, error) {
	if s.ErrorToReturn != nil {
		return model.User{}, s.ErrorToReturn
	}

	for _, v := range s.Users {
		if v.Email == email {
			return v, nil
		}
	}

	return model.User{}, nil
}

func (s UsersRepository) GetByAppleID(appleID string) (model.User, error) {
	for _, v := range s.Users {
		if v.AppleID == appleID {
			return v, nil
		}
	}

	return model.User{}, nil
}

func (s UsersRepository) Save(user model.User) (model.User, error) {
	if user.ID == "" {
		uuid, errUUID := uuid.New()
		if errUUID != nil {
			return model.User{}, errUUID
		}

		user.ID = uuid

	}
	s.Users[user.ID] = user

	return user, nil
}

func (s UsersRepository) IsEmailRegistered(email string) (bool, error) {
	user, err := s.GetByEmail(email)

	if err != nil {
		return false, err
	}

	return user.Email == email, nil
}
