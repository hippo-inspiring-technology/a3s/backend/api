package mocks

import (
	"errors"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/auth"
)

type mockAuthTokenGenerator struct {
	ReturnError bool
	Token       string
}

func (m *mockAuthTokenGenerator) GenerateNewToken(identifier string) (string, error) {
	if m.ReturnError {
		return "", errors.New("Can't generate token")
	}

	return m.Token, nil
}

func NewMockAuthTokenGenerator(returnError bool, tokenToReturn string) auth.AuthTokenGenerator {
	return &mockAuthTokenGenerator{
		ReturnError: returnError,
		Token:       tokenToReturn,
	}
}
