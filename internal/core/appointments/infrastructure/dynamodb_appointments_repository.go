package infrastructure

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/ports"
)

const appointmentsTable = "APPOINTMENTS_STG"

const YYYYMMDD = "2022-12-30"

type dynamoDbAppointmentRepository struct {
	client *dynamodb.DynamoDB
}

//NewAppointmentsRepository retunrs a new DynamoDbRepository
func NewAppointmentsRepository() ports.AppointmentsRepository {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	client := dynamodb.New(sess)

	return &dynamoDbAppointmentRepository{
		client: client,
	}
}

//GetAppointmentsByUserIDAndDate returns an Appointment from DynamoDb
func (r *dynamoDbAppointmentRepository) GetAppointmentsByUserIDAndDate(userID string, date time.Time) ([]model.Appointment, error) {
	appointments := []model.Appointment{}

	guestCondition := expression.Name("GuestID").Equal(expression.Value(userID))
	ownerCondition := expression.Name("OwnerID").Equal(expression.Value(userID))
	dateCondition := expression.Name("Date").Equal(expression.Value(date.Format(YYYYMMDD)))
	proj := expression.NamesList(
		expression.Name("ID"),
		expression.Name("OwnerID"),
		expression.Name("GuestID"),
		expression.Name("Date"),
		expression.Name("Duration"),
	)

	expr, err := expression.NewBuilder().WithCondition((guestCondition.Or(ownerCondition)).And(dateCondition)).WithProjection(proj).Build()
	if err != nil {
		return appointments, err
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(getUsersTable()),
	}

	result, err := r.client.Scan(params)
	if err != nil {
		return appointments, err
	}

	if len(result.Items) == 0 {
		return appointments, nil
	}

	for _, v := range result.Items {
		appointment := model.Appointment{}

		err = dynamodbattribute.UnmarshalMap(v, &appointment)

		if err != nil {
			panic("Failed to unmarshal Record")
		}

		appointments = append(appointments, appointment)
	}

	return appointments, err
}
