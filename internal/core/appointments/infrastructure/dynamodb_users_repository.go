package infrastructure

import (
	"errors"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/ports"
)

const usersTable = "USERS_"

//GetUsersTable
func getUsersTable() string {
	return usersTable + os.Getenv("ENVIRONMENT")
}

type dynamoDbUserRepository struct {
	client *dynamodb.DynamoDB
}

func newDynamoClient() *dynamodb.DynamoDB {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	return dynamodb.New(sess)
}

//NewUsersRepository returns a new instance of the core implementation of users repository
func NewUsersRepository() ports.UsersRepository {
	return &dynamoDbUserRepository{
		client: newDynamoClient(),
	}
}

func (r *dynamoDbUserRepository) GetUserByID(userID string) (model.User, error) {
	user := model.User{}

	filt := expression.Name("ID").Equal(expression.Value(userID))
	proj := expression.NamesList(
		expression.Name("ID"),
		expression.Name("FirstName"),
		expression.Name("LastName"),
		expression.Name("Email"),
		expression.Name("Phone"),
	)

	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		return user, err
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(getUsersTable()),
	}

	result, err := r.client.Scan(params)
	if err != nil {
		return user, err
	}

	if len(result.Items) == 0 {
		return user, nil
	}

	if len(result.Items) > 1 {
		return user, errors.New("More than one user with this ID is registered")
	}

	err = dynamodbattribute.UnmarshalMap(result.Items[0], &user)
	if err != nil {
		panic("Failed to unmarshal Record")
	}

	return user, err
}
