package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/actions"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/ports"
)

const ownerUserID = "db7c503c-f92b-11ea-90a1-c4b301c4553b"
const guestUserID = "db7c503c-f92b-11ea-90a1-c4b301c4553a"

type mockUsersRepository struct {
	users map[string]model.User
}

func (m *mockUsersRepository) GetUserByID(userID string) (model.User, error) {
	user, ok := m.users[userID]

	if !ok {
		return model.User{}, entities.ErrUserNotFound
	}

	return user, nil
}

func NewMockUsersRepository() ports.UsersRepository {
	return &mockUsersRepository{
		users: make(map[string]model.User),
	}
}

func NewMockUsersRepositoryWithUsers() ports.UsersRepository {
	u := make(map[string]model.User)
	u[ownerUserID] = model.User{
		ID:        ownerUserID,
		FirstName: "Carlos",
		LastName:  "Rodrigo",
		Phone:     "+541155667788",
		Email:     "carlos@hippo.tech",
	}
	u[guestUserID] = model.User{
		ID:        guestUserID,
		FirstName: "Lucila",
		LastName:  "Froment",
		Phone:     "+541144556677",
		Email:     "lucila@hippo.tech",
	}

	return &mockUsersRepository{
		users: u,
	}
}

type mockAppointmentsRepository struct {
	appointments []model.Appointment
}

func (m *mockAppointmentsRepository) GetAppointmentsByUserIDAndDate(userID string, date time.Time) ([]model.Appointment, error) {
	results := []model.Appointment{}
	for _, v := range m.appointments {
		if userID == v.OwnerID || userID == v.GuestID {
			if v.Date.Day() == date.Day() {
				results = append(results, v)
			}
		}
	}

	return results, nil
}

func NewMockAppointmentsRepository() ports.AppointmentsRepository {
	return &mockAppointmentsRepository{}
}

func NewMockAppointmentsRepositoryWithAppointments() ports.AppointmentsRepository {
	date := time.Date(2020, time.October, 4, 0, 0, 0, 0, time.UTC)

	appointments := []model.Appointment{
		model.Appointment{
			ID:       "1",
			OwnerID:  ownerUserID,
			GuestID:  guestUserID,
			Date:     date,
			Duration: time.Date(2020, time.October, 4, 1, 0, 0, 0, time.UTC),
		},
	}
	return &mockAppointmentsRepository{
		appointments: appointments,
	}
}

func TestListingAppointmentsHandler(t *testing.T) {
	users := NewMockUsersRepository()
	appointments := NewMockAppointmentsRepository()
	action := actions.NewGetAppointmentsByUserIdAndDateAction(users, appointments)
	handle := NewTestAppointmentsHandler(action)

	t.Run("Given an invalid jwt token on the request, When we want to get appointments for a user and a date, Then should return an error", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/appointments/users/db7c503c-f92b-11ea-90a1-c4b301c4553b?date=2020-12-22", nil)
		req.Header.Add("Authorization", "eyJhbGciOiJIiii1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS1jNGIzMDFjNDU1M2IiLCJleHAiOjI2MDA4MTgzMDl9.64spgJlqoj44EpIX_uopOGza-MKZ8NXFIe2NnlSUcGE")

		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("GET", "/appointments/users/:userID", handle.GetAppointmentsByUserID)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnauthorized {
			t.Errorf("Expected response code to be 401 - Unauthorized but is %v", rr.Code)
		}
	})

	t.Run("Given an valid jwt token on the request, When we want to get appointments for a different user than the token owner, Then should return an error", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/appointments/users/db7c503c-f92b-11ea-90a1-c4b301c4553b?date=2020-12-22", nil)
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.l0M_Y52vWA_blZ9OTWSUHdKIpoojkktnSM8ALihuBbU")

		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("GET", "/appointments/users/:userID", handle.GetAppointmentsByUserID)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnauthorized {
			t.Errorf("Expected response code to be 401 - Unauthorized but is %v, %q", rr.Code, rr.Body)
		}

	})

	t.Run("Given an valid jwt token on the request, When we want to get appointments for the same user than the token owner, Then should return a list of appointments", func(t *testing.T) {
		users := NewMockUsersRepositoryWithUsers()
		appointments := NewMockAppointmentsRepositoryWithAppointments()
		action := actions.NewGetAppointmentsByUserIdAndDateAction(users, appointments)
		handle := NewTestAppointmentsHandler(action)
		req, err := http.NewRequest("GET", "/appointments/users/db7c503c-f92b-11ea-90a1-c4b301c4553b?date=2020-10-04", nil)
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS1jNGIzMDFjNDU1M2IiLCJleHAiOjI2MDA4MTgzMDl9.64spgJlqoj44EpIX_uopOGza-MKZ8NXFIe2NnlSUcGE")

		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("GET", "/appointments/users/:userID", handle.GetAppointmentsByUserID)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("Expected response code to be 200 but is %v", rr.Code)
		}

		expectedBody := `{"code":200,"message":"Successful","data":[{"appointment_id":"1","owner":{"id":"db7c503c-f92b-11ea-90a1-c4b301c4553b","name":"Carlos Rodrigo","phone":"+541155667788","email":"carlos@hippo.tech"},"guest":{"id":"db7c503c-f92b-11ea-90a1-c4b301c4553a","name":"Lucila Froment","phone":"+541144556677","email":"lucila@hippo.tech"},"is_owner":true,"date":"2020-10-04T00:00:00Z","duration":"2020-10-04T01:00:00Z"}]}
`

		assert.Equal(t, expectedBody, rr.Body.String())
	})
}

func NewTestAppointmentsHandler(getByUserIDAndDateAction actions.GetAppointmentsByUserIdAndDateAction) AppointmentsHandler {
	return AppointmentsHandler{
		GetAppointmentsByUserIdAndDate: getByUserIDAndDateAction,
	}
}
