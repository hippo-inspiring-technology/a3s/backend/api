package handlers

import (
	"log"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/actions"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/infrastructure"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/auth"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/handler"
)

type AppointmentsHandler struct {
	GetAppointmentsByUserIdAndDate actions.GetAppointmentsByUserIdAndDateAction
}

const layout = "2006-01-02"

func (h *AppointmentsHandler) GetAppointmentsByUserID(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	claims, errToken := auth.GetClaimsFromToken(r.Header.Get("Authorization"))
	if errToken != nil {
		log.Printf("Error - %q\n", errToken)
		handler.WriteResponseWithoutData(w, http.StatusUnauthorized, errToken.Error())
		return
	}

	dateParam := r.URL.Query().Get("date")
	if dateParam == "" {
		log.Printf("Error - Missing Date in Request")
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Missing Date in Request")
		return
	}
	date, errDate := time.Parse(layout, dateParam)
	if errDate != nil {
		log.Printf("Error - %q\n", errDate)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, errDate.Error())
		return
	}

	userID := p.ByName("userID")
	if userID != claims.UserID {
		handler.WriteResponseWithoutData(w, http.StatusUnauthorized, "Unauthorized user ")
		return
	}

	appointments, err := h.GetAppointmentsByUserIdAndDate.GetAppointments(userID, date)
	if err != nil {
		log.Printf("Error - %q\n", err)
		handler.WriteResponseWithoutData(w, http.StatusInternalServerError, err.Error())
		return
	}

	handler.WriteResponse(w, http.StatusOK, "Successful", appointments)
}

func NewAppointmentsHandler() AppointmentsHandler {
	users := infrastructure.NewUsersRepository()
	appointments := infrastructure.NewAppointmentsRepository()

	return AppointmentsHandler{
		GetAppointmentsByUserIdAndDate: actions.NewGetAppointmentsByUserIdAndDateAction(users, appointments),
	}
}
