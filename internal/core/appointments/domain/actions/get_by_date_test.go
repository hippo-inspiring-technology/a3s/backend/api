package actions

import (
	"testing"
	"time"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/ports"
)

type mockUsersRepository struct {
	users map[string]model.User
}

func (m *mockUsersRepository) GetUserByID(userID string) (model.User, error) {
	user, ok := m.users[userID]

	if !ok {
		return model.User{}, entities.ErrUserNotFound
	}

	return user, nil
}

func NewMockUsersRepository() ports.UsersRepository {
	return &mockUsersRepository{
		users: make(map[string]model.User),
	}
}

func NewMockUsersRepositoryWithUsers(users []model.User) ports.UsersRepository {
	u := make(map[string]model.User)
	for _, v := range users {
		u[v.ID] = v
	}
	return &mockUsersRepository{
		users: u,
	}
}

type mockAppointmentsRepository struct {
	appointments []model.Appointment
}

func (m *mockAppointmentsRepository) GetAppointmentsByUserIDAndDate(userID string, date time.Time) ([]model.Appointment, error) {
	results := []model.Appointment{}
	for _, v := range m.appointments {
		if userID == v.OwnerID || userID == v.GuestID {
			if v.Date.Day() == date.Day() {
				results = append(results, v)
			}
		}
	}

	return results, nil
}

func NewMockAppointmentsRepository() ports.AppointmentsRepository {
	return &mockAppointmentsRepository{}
}

func NewMockAppointmentsRepositoryWithAppointments(appointments []model.Appointment) ports.AppointmentsRepository {
	return &mockAppointmentsRepository{
		appointments: appointments,
	}
}

func TestListingAppointmentsForUserAction(t *testing.T) {
	date := time.Date(2020, time.October, 4, 0, 0, 0, 0, time.UTC)
	ownerUserID := "1"
	guestUserID := "2"
	mockUserOwner := model.User{
		ID:        ownerUserID,
		FirstName: "Carlos",
		LastName:  "Rodrigo",
	}
	mockUserGuest := model.User{
		ID:        guestUserID,
		FirstName: "Carlos",
		LastName:  "Rodrigo",
	}
	mockAppointment := model.Appointment{
		ID:       "1",
		OwnerID:  "1",
		GuestID:  "2",
		Date:     date,
		Duration: time.Date(2020, time.October, 4, 1, 0, 0, 0, time.UTC),
	}

	usersRepo := NewMockUsersRepository()
	appointmentsRepo := NewMockAppointmentsRepository()
	action := NewGetAppointmentsByUserIdAndDateAction(usersRepo, appointmentsRepo)

	t.Run("Given a userID, When userID is not registered, Then should return an error ", func(t *testing.T) {
		userID := "invalid id"

		_, err := action.GetAppointments(userID, date)

		if err == nil {
			t.Errorf("We should have an error here")
		}

		if err.Error() != entities.ErrUserNotFound.Error() {
			t.Errorf("The error is not the expected error")
		}
	})

	t.Run("Given a valid userID, When are appointments availables for that user, Then should return a non empty list of appointments", func(t *testing.T) {
		usersRepo = NewMockUsersRepositoryWithUsers([]model.User{mockUserOwner, mockUserGuest})
		appointmentsRepo = NewMockAppointmentsRepositoryWithAppointments([]model.Appointment{mockAppointment})
		action = NewGetAppointmentsByUserIdAndDateAction(usersRepo, appointmentsRepo)

		ua, err := action.GetAppointments(ownerUserID, date)

		if err != nil {
			t.Errorf("We don't expect an error here, %q", err)
		}

		if len(ua) != 1 {
			t.Errorf("Should return only one user appointment")
		}
	})

	t.Run("Given a valid userID, When the user is the owner of an appointment, Then the appointment should have it as owner", func(t *testing.T) {
		usersRepo = NewMockUsersRepositoryWithUsers([]model.User{mockUserOwner, mockUserGuest})
		appointmentsRepo = NewMockAppointmentsRepositoryWithAppointments([]model.Appointment{mockAppointment})
		action = NewGetAppointmentsByUserIdAndDateAction(usersRepo, appointmentsRepo)

		ua, err := action.GetAppointments(ownerUserID, date)

		if err != nil {
			t.Errorf("We don't expect an error here, %q", err)
		}

		if len(ua) != 1 {
			t.Errorf("Should return only one user appointment")
		}

		if ua[0].Owner.ID != ownerUserID {
			t.Errorf("This user should be the owner")
		}
	})

	t.Run("Given a valid userID, When the user is the guest of an appointment, Then the appointment should have it as guest", func(t *testing.T) {
		usersRepo = NewMockUsersRepositoryWithUsers([]model.User{mockUserOwner, mockUserGuest})
		appointmentsRepo = NewMockAppointmentsRepositoryWithAppointments([]model.Appointment{mockAppointment})
		action = NewGetAppointmentsByUserIdAndDateAction(usersRepo, appointmentsRepo)

		ua, err := action.GetAppointments(guestUserID, date)

		if err != nil {
			t.Errorf("We don't expect an error here, %q", err)
		}

		if len(ua) != 1 {
			t.Errorf("Should return only one user appointment")
		}

		if ua[0].Guest.ID != guestUserID {
			t.Errorf("This user should be the owner")
		}
	})

	t.Run("Given a valid userID and a date, When are appointments for the user, Then the appointment should have the same date", func(t *testing.T) {
		usersRepo = NewMockUsersRepositoryWithUsers([]model.User{mockUserOwner, mockUserGuest})
		appointmentsRepo = NewMockAppointmentsRepositoryWithAppointments([]model.Appointment{mockAppointment})
		action = NewGetAppointmentsByUserIdAndDateAction(usersRepo, appointmentsRepo)

		ua, err := action.GetAppointments(guestUserID, date)

		if err != nil {
			t.Errorf("We don't expect an error here, %q", err)
		}

		if len(ua) != 1 {
			t.Errorf("Should return only one user appointment")
		}

		userAppointmentDate := ua[0].Date

		if !(userAppointmentDate.Year() == date.Year() && userAppointmentDate.YearDay() == date.YearDay()) {
			t.Errorf("The day of the year should be the same, %q, %q", userAppointmentDate, date)
		}
	})
}
