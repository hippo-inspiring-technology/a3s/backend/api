package actions

import (
	"errors"
	"time"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/ports"
)

type GetAppointmentsByUserIdAndDateAction interface {
	GetAppointments(userID string, date time.Time) ([]entities.UserAppointment, error)
}

type getAppointmentsByUserIDAndDateAction struct {
	Users        ports.UsersRepository
	Appointments ports.AppointmentsRepository
}

//NewGetAppointmentsByUserIdAndDateAction returns an instance of GetAppointmentsByDateAction
func NewGetAppointmentsByUserIdAndDateAction(users ports.UsersRepository, appointments ports.AppointmentsRepository) GetAppointmentsByUserIdAndDateAction {
	return &getAppointmentsByUserIDAndDateAction{
		Users:        users,
		Appointments: appointments,
	}
}

func (a *getAppointmentsByUserIDAndDateAction) GetAppointments(userID string, date time.Time) ([]entities.UserAppointment, error) {
	userAppointments := []entities.UserAppointment{}

	user, err := a.Users.GetUserByID(userID)

	if err != nil {
		return userAppointments, err
	}

	appointments, err := a.Appointments.GetAppointmentsByUserIDAndDate(user.ID, date)

	if err != nil {
		return userAppointments, err
	}

	for _, v := range appointments {
		userAppointment := entities.UserAppointment{
			AppointmentID: v.ID,
			Date:          v.Date,
			Duration:      v.Duration,
		}

		if user.ID == v.OwnerID {
			userAppointment.Owner = buildUser(user)
			userAppointment.IsOwner = true

			guest, err := a.Users.GetUserByID(v.GuestID)

			if err != nil {
				return userAppointments, errors.New("Guest user not found")
			}

			userAppointment.Guest = buildUser(guest)
		} else {
			userAppointment.Guest = buildUser(user)
			userAppointment.IsOwner = false

			owner, err := a.Users.GetUserByID(v.OwnerID)

			if err != nil {
				return userAppointments, errors.New("Owner user not found")
			}

			userAppointment.Owner = buildUser(owner)
		}

		userAppointments = append(userAppointments, userAppointment)
	}

	return userAppointments, nil
}

func buildUser(user model.User) entities.User {
	return entities.User{
		ID:    user.ID,
		Name:  user.FirstName + " " + user.LastName,
		Phone: user.Phone,
		Email: user.Email,
	}
}
