package model

//User is the representation of a User in the listing context
type User struct {
	ID        string
	FirstName string
	LastName  string
	Phone     string
	Email     string
}
