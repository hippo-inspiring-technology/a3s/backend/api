package model

import "time"

//Appointment struct is the representation of an appointment
type Appointment struct {
	ID       string
	OwnerID  string
	GuestID  string
	Date     time.Time
	Duration time.Time
}
