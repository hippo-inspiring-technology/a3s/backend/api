package entities

import "time"

type UserAppointment struct {
	AppointmentID string    `json:"appointment_id"`
	Owner         User      `json:"owner"`
	Guest         User      `json:"guest"`
	IsOwner       bool      `json:"is_owner"`
	Date          time.Time `json:"date"`
	Duration      time.Time `json:"duration"`
}
