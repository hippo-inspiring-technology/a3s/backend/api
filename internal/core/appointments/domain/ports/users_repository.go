package ports

import "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/model"

type UsersRepository interface {
	GetUserByID(userID string) (model.User, error)
}
