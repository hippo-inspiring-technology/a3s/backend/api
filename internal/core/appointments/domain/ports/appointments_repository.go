package ports

import (
	"time"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/appointments/domain/model"
)

type AppointmentsRepository interface {
	GetAppointmentsByUserIDAndDate(userID string, date time.Time) ([]model.Appointment, error)
}
