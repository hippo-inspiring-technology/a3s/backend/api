package validation

import (
	"errors"
	"net/mail"
)

//ErrEmailIsRequired is used when email is not provided
var ErrEmailIsRequired = errors.New("Email is required")

//ErrEmailBadFormat is used when email format is not correct
var ErrEmailBadFormat = errors.New("Email format is incorrect")

func ValidateEmail(email string) error {
	if email == "" {
		return ErrEmailIsRequired
	}

	_, err := mail.ParseAddress(email)

	if err != nil {
		return ErrEmailBadFormat
	}

	return nil
}
