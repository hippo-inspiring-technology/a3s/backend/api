package validation

import "errors"

//ErrPasswordIsRequired is used when password is not provided
var ErrPasswordIsRequired = errors.New("Password is required")

//ErrConfirmationPasswordIsRequired is used when confirmation password is not provided
var ErrConfirmationPasswordIsRequired = errors.New("Confirmation password is required")

//ErrPasswordAndConfirmationAreNotEquals is used when password and confirmation password are not the same
var ErrPasswordAndConfirmationAreNotEquals = errors.New("Password and Confirmation Password must be the same")

//ErrPasswordMinLenght is used when password lenght is less than 8 characters
var ErrPasswordMinLenght = errors.New("Password minimun lenght is 8 character")

func ValidatePassword(password string) error {
	if password == "" {
		return ErrPasswordIsRequired
	}

	if len(password) < 8 {
		return ErrPasswordMinLenght
	}

	return nil
}

func ValidateAreTheSamePassword(password, confirmationPassword string) error {
	if password != confirmationPassword {
		return ErrPasswordAndConfirmationAreNotEquals
	}

	return nil
}
