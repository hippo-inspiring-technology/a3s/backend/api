package model

//User representation in profile context
type User struct {
	ID                 string
	FirstName          string
	LastName           string
	Email              string
	Password           string
	AppleID            string
	Country            string
	Phone              string
	Activity           Activity
	PaymentInformation PaymentInformation
}

//PaymentInformation represent the ways of payments that the professional has availables
type PaymentInformation struct {
	BankAccountID    string
	BankAccountAlias string
}

//Activity represents the user activity
type Activity struct {
	Profession         string
	Days               []string
	Hours              []string
	AverageFeeAmount   float32
	DefaultFeeCurrency string
	SessionDuration    float32
}
