package entities

import "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"

//Activity represents the user activity
type Activity struct {
	Profession         string   `json:"profession"`
	Days               []string `json:"days"`
	Hours              []string `json:"hours"`
	AverageFeeAmount   float32  `json:"avg_fee"`
	DefaultFeeCurrency string   `json:"fee_currency"`
	SessionDuration    float32  `json:"session_duration"`
}

func NewActivity(activity model.Activity) Activity {
	return Activity{
		Profession:       activity.Profession,
		Days:             activity.Days,
		Hours:            activity.Hours,
		AverageFeeAmount: activity.AverageFeeAmount,
		SessionDuration:  activity.SessionDuration,
	}
}
