package entities

import "errors"

var ErrEmailIsAlreadyRegistered = errors.New("Email is already registered")
var ErrUserNotFound = errors.New("User not found")

//ErrUserNotRegistered is triggered when the user is not registered yet
var ErrUserNotRegistered = errors.New("User not Registered")

//ErrStorageNotAccesible is triggered when storage layer is not accesible
var ErrStorageNotAccesible = errors.New("Can't access to storage")

//ErrPasswordDoesntMatch is triggered when password does not match with the stored
var ErrPasswordDoesntMatch = errors.New("Password is incorrect")
