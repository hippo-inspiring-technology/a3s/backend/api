package entities

import "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"

type User struct {
	ID                 string             `json:"id"`
	FirstName          string             `json:"first_name"`
	LastName           string             `json:"last_name"`
	Email              string             `json:"email"`
	Country            string             `json:"country"`
	Phone              string             `json:"phone"`
	Activity           Activity           `json:"activity,omitempty"`
	PaymentInformation PaymentInformation `json:"payment_information,omitempty"`
	AccessToken        string             `json:"access_token"`
}

func NewUser(user model.User) User {
	return User{
		ID:                 user.ID,
		FirstName:          user.FirstName,
		LastName:           user.LastName,
		Email:              user.Email,
		Country:            user.Country,
		Phone:              user.Phone,
		Activity:           NewActivity(user.Activity),
		PaymentInformation: NewPaymentInformation(user.PaymentInformation),
	}
}
