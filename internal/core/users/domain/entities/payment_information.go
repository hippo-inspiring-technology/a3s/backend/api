package entities

import "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"

//PaymentInformation represent the ways of payments that the professional has availables
type PaymentInformation struct {
	BankAccountID    string `json:"bank_account_id"`
	BankAccountAlias string `json:"bank_account_alias"`
}

//IsValid retunrs if the payment information is valid or not
func (pi *PaymentInformation) IsValid() bool {
	return pi.BankAccountID != "" || pi.BankAccountAlias != ""
}

func NewPaymentInformation(paymentInformation model.PaymentInformation) PaymentInformation {
	return PaymentInformation{
		BankAccountID:    paymentInformation.BankAccountID,
		BankAccountAlias: paymentInformation.BankAccountAlias,
	}
}
