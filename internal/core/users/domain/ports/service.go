package ports

import "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"

type UsersService interface {
	Register(email, password string) (entities.User, error)
	Login(email, password string) (entities.User, error)
}
