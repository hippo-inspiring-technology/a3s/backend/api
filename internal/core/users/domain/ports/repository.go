package ports

import "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"

type UsersRepository interface {
	Update(u model.User) error
	Exist(ID string) (bool, error)
	GetByID(ID string) (model.User, error)
	GetByAppleID(appleID string) (model.User, error)
	GetByEmail(email string) (model.User, error)
	IsEmailRegistered(email string) (bool, error)
	Save(u model.User) (model.User, error)
}
