package services

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/hash"
	stub "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/test/stub/users"
)

func InitServiceWithEmptyRepo() ports.UsersService {
	return NewUsersService(stub.UsersRepository{
		Users: map[string]model.User{},
	})
}

func InitServiceWithError(e error) ports.UsersService {
	return NewUsersService(stub.UsersRepository{
		Users:         map[string]model.User{},
		ErrorToReturn: e,
	})
}

func InitServiceWithUsers(u model.User) ports.UsersService {
	users := make(map[string]model.User)
	users[u.ID] = u

	return NewUsersService(stub.UsersRepository{
		Users: users,
	})
}
func TestCompleateInfoCase(t *testing.T) {
	service := InitServiceWithEmptyRepo()
	email := "carlos@hippo.dev"
	password := "carlos"

	_, err := service.Register(email, password)

	assert.Nil(t, err)
}

func TestEmailIsAlreadyTaken(t *testing.T) {
	email := "carlos@hippo.dev"
	password := "carlos"
	service := InitServiceWithUsers(model.User{
		ID:       "123",
		Email:    email,
		Password: password,
	})

	_, err := service.Register(email, password)

	assert.NotNil(t, err)
	assert.Equal(t, err, entities.ErrEmailIsAlreadyRegistered)
}

func TestRepositoryThrowsErrors(t *testing.T) {
	errMock := errors.New("Intentional Error")
	email := "carlos@hippo.dev"
	password := "carlos"
	service := InitServiceWithError(errMock)

	_, err := service.Register(email, password)

	if err == nil {
		t.Errorf("We expect an error here, %q", err)
	}

	if err == entities.ErrEmailIsAlreadyRegistered {
		t.Errorf("We expect %q and got %q", errMock, err)
	}
}

func TestLogin(t *testing.T) {
	var email = "carlos@hippo.dev"
	var password = "password"

	t.Run("Given a not registered User, when try to login, then should recieve an error", func(t *testing.T) {
		service := InitServiceWithEmptyRepo()

		_, err := service.Login(email, password)
		expectedError := entities.ErrUserNotFound

		assert.NotNil(t, err)
		assert.Equal(t, expectedError, err)
	})
	t.Run("Given a registered User, when try to login but storage is not accesible, then should recieve an error", func(t *testing.T) {
		expectedError := entities.ErrStorageNotAccesible
		service := InitServiceWithError(expectedError)

		_, err := service.Login(email, password)

		assert.NotNil(t, err)
		assert.Equal(t, expectedError, err)
	})
	t.Run("Given a registered user with a wrong password, when try to login, then should receive an error", func(t *testing.T) {
		userMock := model.User{
			ID:       "123",
			Email:    email,
			Password: "none",
		}
		service := InitServiceWithUsers(userMock)

		_, err := service.Login(email, password)
		expectedError := entities.ErrPasswordDoesntMatch

		assert.NotNil(t, err)
		assert.Equal(t, expectedError, err)
	})

	t.Run("Given a registered user with valid credentias, when try to login, then should return a valid user with email and ID", func(t *testing.T) {
		mockedPassword, _ := hash.Generate(password)

		userMock := model.User{
			ID:       "123",
			Email:    email,
			Password: mockedPassword,
		}
		service := InitServiceWithUsers(userMock)

		loggedUserInfo, err := service.Login(email, password)

		assert.Nil(t, err)
		assert.NotEqual(t, entities.User{}, loggedUserInfo)
		assert.Equal(t, userMock.ID, loggedUserInfo.ID)
		assert.Equal(t, userMock.Email, loggedUserInfo.Email)
	})
}
