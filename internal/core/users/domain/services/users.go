package services

import (
	"log"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/hash"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/uuid"
)

type service struct {
	Users ports.UsersRepository
}

func (s *service) Login(email, password string) (entities.User, error) {
	userFromDb, errGetUser := s.Users.GetByEmail(email)
	user := entities.NewUser(userFromDb)

	if errGetUser != nil {
		return user, errGetUser
	}

	if user.ID == "" {
		return user, entities.ErrUserNotFound
	}

	errPasswordComparition := hash.Compare(userFromDb.Password, password)
	if errPasswordComparition != nil {
		return user, entities.ErrPasswordDoesntMatch
	}

	return user, nil
}

func (s *service) Register(email, password string) (entities.User, error) {
	user := entities.User{}

	isRegistered, errIsRegistered := s.Users.IsEmailRegistered(email)
	if errIsRegistered != nil {
		log.Println(errIsRegistered)
		return user, errIsRegistered
	}

	if isRegistered {
		return user, entities.ErrEmailIsAlreadyRegistered
	}

	hashedPassword, errHashingPassword := hash.Generate(password)
	if errHashingPassword != nil {
		return user, errHashingPassword
	}

	uuid, errUUID := uuid.New()
	if errUUID != nil {
		return user, errUUID
	}

	userToSave := model.User{
		ID:       uuid,
		Email:    email,
		Password: hashedPassword,
	}

	userSaved, errSavingUser := s.Users.Save(userToSave)
	user = entities.NewUser(userSaved)

	if errSavingUser != nil {
		return user, errSavingUser
	}

	return user, nil
}

//New return an implementation of Users Service
func NewUsersService(r ports.UsersRepository) ports.UsersService {
	return &service{
		Users: r,
	}
}
