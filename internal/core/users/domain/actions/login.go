package actions

import (
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/validation"
)

//Action interface represents the Login action behavior
type LoginAction interface {
	Login(email, password string) (entities.User, error)
}

type loginAction struct {
	service ports.UsersService
}

//NewLoginAction returns a new Action
func NewLoginAction(s ports.UsersService) LoginAction {
	return &loginAction{
		service: s,
	}
}

func (a *loginAction) Login(email, password string) (entities.User, error) {
	loggedUser := entities.User{}
	errEmail := validation.ValidateEmail(email)
	if errEmail != nil {
		return loggedUser, errEmail
	}

	errPassword := validation.ValidatePassword(password)
	if errPassword != nil {
		return loggedUser, errPassword
	}

	user, errLogin := a.service.Login(email, password)
	if errLogin != nil {
		return loggedUser, errLogin
	}

	return user, nil
}
