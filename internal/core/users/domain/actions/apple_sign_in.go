package actions

import (
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/validation"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/uuid"
)

type AppleSignInAction interface {
	SignIn(appleID string, email string, firstName string, lastName string) (entities.User, error)
}

type appleSignInAction struct {
	Users ports.UsersRepository
}

func (a *appleSignInAction) SignIn(appleID string, email string, firstName string, lastName string) (entities.User, error) {
	user := entities.User{}

	errEmail := validation.ValidateEmail(email)
	if errEmail != nil {
		return user, errEmail
	}

	userFromApple, errGetUser := a.Users.GetByAppleID(appleID)
	if errGetUser != nil {
		return user, errGetUser
	}

	if userFromApple.ID == "" {
		uuid, errUUID := uuid.New()
		if errUUID != nil {
			return user, errUUID
		}

		userUpdated, errSave := a.Users.Save(model.User{
			ID:        uuid,
			AppleID:   appleID,
			Email:     email,
			FirstName: firstName,
			LastName:  lastName,
		})

		if errSave != nil {
			return user, errSave
		}

		return entities.NewUser(userUpdated), nil
	}

	return entities.NewUser(userFromApple), nil
}

func NewAppleSignInAction(users ports.UsersRepository) AppleSignInAction {
	return &appleSignInAction{
		Users: users,
	}
}
