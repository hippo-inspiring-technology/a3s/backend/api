package actions

import (
	"testing"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	stub "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/test/stub/users"
)

func TestAppleSignInLoginUser(t *testing.T) {
	t.Run("Given a UserID, email, When emails is not  valid email, Then should return an error", func(t *testing.T) {
		appleID := "Testing ID"
		email := "test@.com"

		action := InitAppleSignInAction()

		_, err := action.SignIn(appleID, email, "", "")

		if err == nil {
			t.Errorf("expected an error here")
		}
	})
	t.Run("Given a valid email and an UserID, When the UserID is not registered, Then should return a new user", func(t *testing.T) {
		appleID := "Testing ID"
		email := "valid@email.com"

		action := InitAppleSignInAction()

		userDto, err := action.SignIn(appleID, email, "", "")

		if err != nil {
			t.Errorf("Expected not errors here, but get %v", err)
		}

		if userDto.Email != email {
			t.Errorf("Email must be the same as the provided one, but is %s", userDto.Email)
		}

		if userDto.ID == "" {
			t.Errorf("ID must be not empty")
		}
	})
	t.Run("Given a firts and last name, When the user is not registered, Then should return an user with those names", func(t *testing.T) {
		appleID := "Apple ID"
		email := "valid@email.com"
		firstName := "Carlos"
		lastName := "Rodrigo"

		action := InitAppleSignInAction()

		userDto, err := action.SignIn(appleID, email, firstName, lastName)

		if err != nil {
			t.Errorf("Expected not errors here, but get %v", err)
		}

		if userDto.FirstName != firstName {
			t.Errorf("FirstName must be the same as the provided one, but is %s", userDto.FirstName)
		}

		if userDto.LastName != lastName {
			t.Errorf("LastName must be the same as the provided one, but is %s", userDto.LastName)
		}
	})
	t.Run("Given a UserID, When the user is registered, Then should return the existed user informaiton", func(t *testing.T) {
		expectedUser := model.User{
			AppleID:   "apple ID",
			ID:        "user ID",
			Email:     "test@valid.com",
			FirstName: "Carlos",
			LastName:  "Rodrigo",
		}

		action := InitActionWithUser(expectedUser)

		userDto, err := action.SignIn(expectedUser.AppleID, expectedUser.Email, "", "")

		if err != nil {
			t.Errorf("An error is not expected here")
		}

		if userDto.ID != expectedUser.ID {
			t.Errorf("ID must be %s but is %s", expectedUser.ID, userDto.ID)
		}

		if userDto.Email != expectedUser.Email {
			t.Errorf("Email must be %s but is %s", expectedUser.Email, userDto.Email)
		}

		if userDto.FirstName != expectedUser.FirstName {
			t.Errorf("FirstName must be %s but is %s", expectedUser.FirstName, userDto.FirstName)
		}

		if userDto.LastName != expectedUser.LastName {
			t.Errorf("LastName must be %s but is %s", expectedUser.LastName, userDto.LastName)
		}

	})
}

func InitAppleSignInAction() AppleSignInAction {
	return NewAppleSignInAction(stub.UsersRepository{
		Users: map[string]model.User{},
	})
}

func InitActionWithUser(user model.User) AppleSignInAction {
	users := make(map[string]model.User)
	users[user.ID] = user

	return NewAppleSignInAction(stub.UsersRepository{
		Users: users,
	})
}
