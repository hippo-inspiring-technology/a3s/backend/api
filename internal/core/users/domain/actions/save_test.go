package actions

import (
	"testing"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	stub "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/test/stub/users"
)

func TestSaveProfileAction(t *testing.T) {
	t.Run("Given a user profile, When the user not exist, Then should return an error", func(t *testing.T) {
		action := InitAction()
		user := entities.User{
			ID: "123",
		}

		err := action.SaveUser(user)

		if err == nil {
			t.Errorf("Error expected here - %v", err)
		}
	})
	t.Run("Given a user profile, When the user exist, Then should not return an error", func(t *testing.T) {
		user := entities.User{
			ID: "123",
		}
		action := InitActionWithMock(stub.UsersRepository{
			Users: map[string]model.User{"123": NewUser(user)},
		})

		err := action.SaveUser(user)

		if err != nil {
			t.Errorf("Error is not expected - %v", err)
		}
	})
	t.Run("Given a user profile, When can't access to the storage when checking if exist, Then should return an error", func(t *testing.T) {
		user := entities.User{
			ID: "123",
		}
		action := InitActionWithMock(stub.UsersRepository{
			ErrorInExist: true,
		})
		expectedError := entities.ErrStorageNotAccesible

		err := action.SaveUser(user)

		if err.Error() != expectedError.Error() {
			t.Errorf("Error is not the expected error - %v", err)
		}
	})

	t.Run("Given a user profile, When can't access to the storage when save the profile, Then should return an error", func(t *testing.T) {
		user := entities.User{
			ID: "123",
		}
		action := InitActionWithMock(stub.UsersRepository{
			ErrorInExist: false,
			ErrorInSave:  true,
			Users:        map[string]model.User{"123": NewUser(user)},
		})
		expectedError := entities.ErrStorageNotAccesible

		err := action.SaveUser(user)

		if err.Error() != expectedError.Error() {
			t.Errorf("Error is not the expected error - %v", err)
		}
	})
}

func InitAction() SaveAction {
	return NewSaveUserAction(stub.UsersRepository{})
}
func InitActionWithMock(m stub.UsersRepository) SaveAction {
	return NewSaveUserAction(m)
}
