package actions

import (
	"testing"

	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	stub "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/test/stub/users"
)

func TestGettingUsersAction(t *testing.T) {
	t.Run("Given an userID, When exist a user with that ID, then should return the User", func(t *testing.T) {
		userID := "VALID-USER-ID"
		action := InitGettingUserActionWithMock(stub.UsersRepository{
			Users: map[string]model.User{userID: model.User{
				ID:        userID,
				FirstName: "Carlos",
				LastName:  "Rodrigo",
				Country:   "Argentina",
				Phone:     "+5491158051765",
			}},
		})

		user, err := action.GetUser(userID)

		if err != nil {
			t.Error("Error not expected")
		}

		if user.ID != userID {
			t.Error("UserID should be the same as the one requested")
		}

		validateNotEmpty(user.FirstName, "FirstName should not be empty", t)
		validateNotEmpty(user.LastName, "FirstName should not be empty", t)
		validateNotEmpty(user.Country, "FirstName should not be empty", t)
		validateNotEmpty(user.Phone, "FirstName should not be empty", t)
	})

	t.Run("Given an userID, When not exist a user with that ID, Then should return an error", func(t *testing.T) {
		userID := "INVALID-USER-ID"
		action := InitGettingUserAction()

		_, err := action.GetUser(userID)

		if err == nil {
			t.Error("It should be an error here")
		}

		if err.Error() != entities.ErrUserNotRegistered.Error() {
			t.Error("Error is not the expected error")
		}

	})
}

func InitGettingUserActionWithMock(m stub.UsersRepository) GetAction {
	return NewGetUserAction(m)
}
func InitGettingUserAction() GetAction {
	return NewGetUserAction(stub.UsersRepository{
		Users: map[string]model.User{},
	})
}

func validateNotEmpty(value string, errorMessage string, t *testing.T) {
	if value == "" {
		t.Error(errorMessage)
	}
}
