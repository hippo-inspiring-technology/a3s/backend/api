package actions

import (
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/validation"
)

type RegisterAction interface {
	Register(email, password, confirmationPassword string) (entities.User, error)
}

//RegisterUserAction is a struct able to Register a new user
type registerAction struct {
	service ports.UsersService
}

//NewRegisterUserAction create a new RegisterUserAction
func NewRegisterUserAction(s ports.UsersService) RegisterAction {
	return &registerAction{
		service: s,
	}
}

func (a *registerAction) Register(email, password, confirmationPassword string) (entities.User, error) {
	user := entities.User{}
	errEmail := validation.ValidateEmail(email)
	if errEmail != nil {
		return user, errEmail
	}

	errPassword := validation.ValidatePassword(password)
	if errPassword != nil {
		return user, errPassword
	}

	errConfirmPassword := validation.ValidatePassword(confirmationPassword)
	if errConfirmPassword != nil {
		return user, errConfirmPassword
	}

	errSamePassword := validation.ValidateAreTheSamePassword(password, confirmationPassword)
	if errSamePassword != nil {
		return user, errSamePassword
	}

	user, errService := a.service.Register(email, password)
	if errService != nil {
		return user, errService
	}

	return user, nil
}
