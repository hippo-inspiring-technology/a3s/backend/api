package actions

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/validation"
)

type loginServiceMock struct {
	mock.Mock
	ReturnError bool
}

func (m *loginServiceMock) Login(email, password string) (entities.User, error) {
	args := m.Called(email, password)
	return args.Get(0).(entities.User), args.Error(1)
}

func (m *loginServiceMock) Register(email, password string) (entities.User, error) {
	if m.ReturnError {
		return entities.User{}, errors.New("Expected error")
	}

	return entities.User{}, nil
}

func TestLoginUserActionValidateParams(t *testing.T) {
	var service = new(loginServiceMock)
	var action = NewLoginAction(service)

	t.Run("An User with an empty email should fail", func(t *testing.T) {
		email := ""
		password := "carlos"

		_, err := action.Login(email, password)

		assert.NotNil(t, err)
		assert.Equal(t, validation.ErrEmailIsRequired, err)
	})

	t.Run("An User with a not empty email but not valid format should return an error", func(t *testing.T) {
		email := "carlos"
		password := "password"

		_, err := action.Login(email, password)

		assert.NotNil(t, err)
		assert.Equal(t, validation.ErrEmailBadFormat, err)
	})

	t.Run("An User with an empty password can't login and should recieve and error", func(t *testing.T) {
		email := "carlos@hip.po.it"
		password := ""

		_, err := action.Login(email, password)

		assert.NotNil(t, err)
		assert.Equal(t, validation.ErrPasswordIsRequired, err)
	})
}

func TestLoginUserActionValidateRegisterdUsers(t *testing.T) {
	var service = new(loginServiceMock)
	var action = NewLoginAction(service)

	t.Run("A not register user who want to login should receive an error", func(t *testing.T) {
		email := "carlos@hip.po.it"
		password := "password"

		errExpected := errors.New("User not Registered")
		service.On("Login", email, password).Return(entities.User{}, errExpected)
		_, err := action.Login(email, password)

		assert.NotNil(t, err)
		assert.Equal(t, errExpected, err)
		service.AssertExpectations(t)
	})

	t.Run("Given a registered user, when try to login with right credentias, then get an access token", func(t *testing.T) {
		email := "carlos@hip.po.it"
		password := "password"
		service := new(loginServiceMock)
		action := NewLoginAction(service)

		service.On("Login", email, password).Return(entities.User{
			ID:    "123",
			Email: "carlos@hip.po.it",
		}, nil)

		loggedUser, err := action.Login(email, password)

		assert.Nil(t, err)
		assert.NotEqual(t, "", loggedUser.ID)
		assert.NotEqual(t, "", loggedUser.Email)
		service.AssertExpectations(t)
	})
}
