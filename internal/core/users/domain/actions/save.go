package actions

import (
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
)

//SaveAction interface represent the action to execute
type SaveAction interface {
	SaveUser(p entities.User) error
}

type saveAction struct {
	users ports.UsersRepository
}

func (a *saveAction) SaveUser(u entities.User) error {
	exist, errExist := a.users.Exist(u.ID)
	if errExist != nil {
		return entities.ErrStorageNotAccesible
	}

	if !exist {
		return entities.ErrUserNotRegistered
	}

	errSave := a.users.Update(NewUser(u))
	if errSave != nil {
		return entities.ErrStorageNotAccesible
	}

	return nil
}

//NewSavingUserAction retunrs a new Action
func NewSaveUserAction(users ports.UsersRepository) SaveAction {
	return &saveAction{
		users: users,
	}
}

func NewUser(user entities.User) model.User {
	return model.User{
		ID:                 user.ID,
		FirstName:          user.FirstName,
		LastName:           user.LastName,
		Email:              user.Email,
		Country:            user.Country,
		Phone:              user.Phone,
		Activity:           NewActivity(user.Activity),
		PaymentInformation: NewPaymentInformation(user.PaymentInformation),
	}
}

func NewPaymentInformation(paymentInformation entities.PaymentInformation) model.PaymentInformation {
	return model.PaymentInformation{
		BankAccountID:    paymentInformation.BankAccountID,
		BankAccountAlias: paymentInformation.BankAccountAlias,
	}
}

func NewActivity(activity entities.Activity) model.Activity {
	return model.Activity{
		Profession:       activity.Profession,
		Days:             activity.Days,
		Hours:            activity.Hours,
		AverageFeeAmount: activity.AverageFeeAmount,
		SessionDuration:  activity.SessionDuration,
	}
}
