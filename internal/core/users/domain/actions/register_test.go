package actions

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/validation"
)

type serviceMock struct {
	mock.Mock
	ReturnError bool
}

func (m *serviceMock) Login(email, password string) (entities.User, error) {
	args := m.Called(email, password)
	return args.Get(0).(entities.User), args.Error(1)
}

func (m *serviceMock) Register(email, password string) (entities.User, error) {
	if m.ReturnError {
		return entities.User{}, errors.New("Expected error")
	}

	return entities.User{}, nil
}

func TestEmailValidation(t *testing.T) {
	var service = new(serviceMock)
	var action = NewRegisterUserAction(service)

	t.Run("An User must have email to be registered", func(t *testing.T) {
		email := ""
		password := ""
		confirmationPassword := ""

		_, err := action.Register(email, password, confirmationPassword)

		assert.NotNil(t, err)
		assert.Equal(t, err, validation.ErrEmailIsRequired)
	})

	t.Run("An User with a valid email should not had an Email related error", func(t *testing.T) {
		email := "carlos@hippo.dev"
		password := ""
		confirmationPassword := ""

		_, err := action.Register(email, password, confirmationPassword)

		if err == validation.ErrEmailBadFormat || err == validation.ErrEmailIsRequired {
			t.Errorf("We not expect an error here")
		}
	})

	t.Run("An User with a not empty email but not valid email format should have an error", func(t *testing.T) {
		email := "carlos"
		password := ""
		confirmationPassword := ""

		_, err := action.Register(email, password, confirmationPassword)

		assert.NotNil(t, err)
		assert.Equal(t, err, validation.ErrEmailBadFormat)
	})
}

func TestPasswordValidation(t *testing.T) {
	var service = new(serviceMock)
	var action = NewRegisterUserAction(service)

	t.Run("An user without a password cant be registered", func(t *testing.T) {
		email := "carlos@hippo.dev"
		password := ""
		confirmationPassword := ""

		_, err := action.Register(email, password, confirmationPassword)

		assert.NotNil(t, err)
		assert.Equal(t, err, validation.ErrPasswordIsRequired)
	})

	t.Run("An user without a confirmation password cant be registered", func(t *testing.T) {
		email := "carlos@hippo.dev"
		password := "12345679s"
		confirmationPassword := ""

		_, err := action.Register(email, password, confirmationPassword)

		assert.NotNil(t, err)
		assert.Equal(t, validation.ErrPasswordIsRequired, err)
	})

	t.Run("An user password and confirmation password must be the same to be registered", func(t *testing.T) {
		email := "carlos@hippo.dev"
		password := "carlos1234"
		confirmationPassword := "testtest2"

		_, err := action.Register(email, password, confirmationPassword)

		assert.NotNil(t, err)
		assert.Equal(t, validation.ErrPasswordAndConfirmationAreNotEquals, err)
	})
}

func TestPassworMustBeHashedBeforeRegisterUser(t *testing.T) {
	service := new(serviceMock)
	action := NewRegisterUserAction(service)

	email := "carlos@hippo.dev"
	password := "carlos123"
	confirmationPassword := "carlos123"

	_, err := action.Register(email, password, confirmationPassword)

	assert.Nil(t, err)
}
