package actions

import (
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
)

//GetAction represents the action of Get a user by ID
type GetAction interface {
	GetUser(userID string) (entities.User, error)
}

type getAction struct {
	users ports.UsersRepository
}

func (a *getAction) GetUser(userID string) (entities.User, error) {
	user := entities.User{}
	exist, errExist := a.users.Exist(userID)
	if errExist != nil {
		return user, entities.ErrStorageNotAccesible
	}

	if !exist {
		return user, entities.ErrUserNotRegistered
	}

	userFromDb, errGetUser := a.users.GetByID(userID)

	if errGetUser != nil {
		return user, errGetUser
	}

	return entities.NewUser(userFromDb), nil
}

//NewGettingUserAction return a new GetAction
func NewGetUserAction(users ports.UsersRepository) GetAction {
	return &getAction{
		users: users,
	}
}
