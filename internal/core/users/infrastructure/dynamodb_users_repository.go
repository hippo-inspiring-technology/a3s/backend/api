package users

import (
	"errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
)

const usersTable = "USERS_STG"

//GetUsersTable
func GetUsersTable() string {
	return usersTable
}

//DynamoDbUsersRepository is the dynamoDB representation of the login users repository
type DynamoDbUsersRepository struct {
	client *dynamodb.DynamoDB
}

func newDynamoClient() *dynamodb.DynamoDB {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	return dynamodb.New(sess)
}

//NewUsersRepository returns a new instance of the core implementation of users repository
func NewUsersRepository() *DynamoDbUsersRepository {
	return &DynamoDbUsersRepository{
		client: newDynamoClient(),
	}
}

func (r *DynamoDbUsersRepository) getBy(field, value string) (model.User, error) {
	user := model.User{}

	filt := expression.Name(field).Equal(expression.Value(value))
	proj := expression.NamesList(
		expression.Name("ID"),
		expression.Name("AppleID"),
		expression.Name("FirstName"),
		expression.Name("LastName"),
		expression.Name("Email"),
		expression.Name("Password"),
		expression.Name("Country"),
		expression.Name("Activity"),
		expression.Name("Phone"),
		expression.Name("PaymentInformation"))

	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		return user, err
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(GetUsersTable()),
	}

	result, err := r.client.Scan(params)

	if err != nil {
		return user, err
	}

	if len(result.Items) == 0 {
		return user, nil
	}

	if len(result.Items) > 1 {
		return user, errors.New("More than one user with this ID is registered")
	}

	err = dynamodbattribute.UnmarshalMap(result.Items[0], &user)
	if err != nil {
		panic("Failed to unmarshal Record")
	}

	return user, err

}

func (r *DynamoDbUsersRepository) GetByAppleID(appleID string) (model.User, error) {
	return r.getBy("AppleID", appleID)
}

//IsEmailRegistered checks if the email is already registered
func (r *DynamoDbUsersRepository) IsEmailRegistered(email string) (bool, error) {
	user, err := r.getBy("Email", email)

	if err != nil {
		return true, err
	}

	if user.Email != "" {
		return true, nil
	}

	return false, nil
}

//Save saves in storage a new user
func (r *DynamoDbUsersRepository) Save(u model.User) (model.User, error) {
	av, err := dynamodbattribute.MarshalMap(u)
	if err != nil {
		return u, err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(GetUsersTable()),
	}

	_, err = r.client.PutItem(input)
	if err != nil {
		return u, err
	}

	return u, nil
}

//Save a user into the storage
func (r *DynamoDbUsersRepository) Update(u model.User) error {
	av, err := dynamodbattribute.MarshalMap(u)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(GetUsersTable()),
	}

	_, err = r.client.PutItem(input)
	if err != nil {
		return err
	}

	return nil
}

//Exist check if the userID exists
func (r *DynamoDbUsersRepository) Exist(userID string) (bool, error) {
	filt := expression.Name("ID").Equal(expression.Value(userID))
	proj := expression.NamesList(expression.Name("ID"))
	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		return false, err
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(GetUsersTable()),
	}

	result, err := r.client.Scan(params)
	if err != nil {
		return false, err
	}

	if len(result.Items) == 0 {
		return false, nil
	}

	if len(result.Items) > 1 {
		return false, errors.New("More than one user with this ID is registered")
	}

	return true, nil
}

//GetByEmail return a User
func (r *DynamoDbUsersRepository) GetByEmail(email string) (model.User, error) {
	return r.getBy("Email", email)
}

//GetByID return a User
func (r *DynamoDbUsersRepository) GetByID(userID string) (model.User, error) {
	return r.getBy("ID", userID)
}
