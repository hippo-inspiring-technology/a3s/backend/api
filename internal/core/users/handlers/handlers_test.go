package handlers

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/actions"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/model"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/ports"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/services"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/auth"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/hash"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/test/mocks"
	stub "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/test/stub/users"
)

func TestLoginUserWithAppleIdRequestValidation(t *testing.T) {
	t.Run("Given a body without auth_code, When we want to login with apple sign in, Then should return error", func(t *testing.T) {
		handler := InitHandler(true, false, "")
		router := httprouter.New()
		router.Handle("POST", "/users/signin-with-apple", handler.SignInWithApple)
		rr := httptest.NewRecorder()

		req, err := http.NewRequest("POST", "/users/signin-with-apple", nil)
		if err != nil {
			t.Error(err.Error())
		}
		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusBadRequest {
			t.Errorf("Expected an error when the request don't have an access_code")
		}
	})
	t.Run("Given a body with an auth_code, When the code is invalid, Then should return an error", func(t *testing.T) {
		handler := InitHandler(true, false, "")
		router := httprouter.New()
		router.Handle("POST", "/users/signin-with-apple", handler.SignInWithApple)
		rr := httptest.NewRecorder()

		body := "{\"auth_code\":\"invalid code\"}\n"
		req, err := http.NewRequest("POST", "/users/signin-with-apple", bytes.NewBufferString(body))

		if err != nil {
			t.Error(err.Error())
		}

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnauthorized {
			t.Errorf("Expected an error when the auth_code is invalid")
			t.Error(rr.Body)
		}
	})
}

func TestLoginWithAppleSignInUserCreation(t *testing.T) {
	t.Run("Given a valid auth_code, When the user is not registered, Then return an user and a JWT token", func(t *testing.T) {
		handler := InitHandler(false, false, "")
		router := httprouter.New()
		router.Handle("POST", "/users/signin-with-apple", handler.SignInWithApple)
		rr := httptest.NewRecorder()

		body := "{\"auth_code\":\"valid code\"}\n"
		req, err := http.NewRequest("POST", "/users/signin-with-apple", bytes.NewBufferString(body))

		if err != nil {
			t.Error(err.Error())
		}

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("Expected successful response code")
		}
	})
}
func TestGettingUser(t *testing.T) {
	validUserID := "db7c503c-f92b-11ea-90a1-c4b301c4553b"
	handle := InitHandler(false, false, "")

	t.Run("Given a userID without an accessToken, when we want to get the user info, then should return an authorization error ", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/users/"+validUserID, nil)
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("GET", "/users/:userID", handle.GetUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnauthorized {
			t.Errorf("Expected response code to be 401 - Unauthorized but is %v", rr.Code)
		}
	})
	t.Run("Given a userID and a not valid accessToken, when we want to get the user info, then should return an authorization error ", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/users/"+validUserID, nil)
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.adasdasdasdasdasdasdadadasdasdasdadsassdasdasdasdadasdad")
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("GET", "/users/:userID", handle.GetUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnauthorized {
			t.Errorf("Expected response code to be 401 - Unauthorized but is %v", rr.Code)
		}
	})
	t.Run("Given a not exist userID, when we want to get the user info, then should return an authorization error ", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/users/db7c503c-f92b-11ea-90a1-c4b301c4553c", nil)
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.l0M_Y52vWA_blZ9OTWSUHdKIpoojkktnSM8ALihuBbU")
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("GET", "/users/:userID", handle.GetUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusNotFound {
			t.Errorf("Expected response code to be 404 - Unauthorized but is %v", rr.Code)
		}
	})
	t.Run("Given a existing userID, when we want to get the user info, then should return the user", func(t *testing.T) {
		user := model.User{
			ID:        "db7c503c-f92b-11ea-90a1-c4b301c4553c",
			FirstName: "Carlos",
			LastName:  "Rodrigo",
			Country:   "Argentina",
			Phone:     "+541158051765",
			Activity: model.Activity{
				Profession:         "Psychologist",
				Days:               []string{"Monday", "Friday"},
				Hours:              []string{"8", "18"},
				AverageFeeAmount:   33.4,
				DefaultFeeCurrency: "American Dollars",
				SessionDuration:    45,
			},
			PaymentInformation: model.PaymentInformation{
				BankAccountID:    "bank id",
				BankAccountAlias: "bank alias",
			},
		}
		handle = InitHandlerWithUsers(false, false, "", map[string]model.User{user.ID: user})

		req, err := http.NewRequest("GET", "/users/db7c503c-f92b-11ea-90a1-c4b301c4553c", nil)
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.l0M_Y52vWA_blZ9OTWSUHdKIpoojkktnSM8ALihuBbU")
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("GET", "/users/:userID", handle.GetUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("Expected response code to be 404 - Unauthorized but is %v", rr.Code)
		}
	})
}
func TestLoginUserHandler(t *testing.T) {
	var password, _ = hash.Generate("carlos123")
	var user = model.User{
		ID:       "123",
		Email:    "hi@carlosrodrigo.me",
		Password: password,
	}

	t.Run("Given an email and password, when the login is sucessful, then the status code should be 200", func(t *testing.T) {
		body := "{\"email\":\"hi@carlosrodrigo.me\",\"password\":\"carlos123\"}\n"
		handler := InitHandlerWithUsers(false, false, "", map[string]model.User{user.Email: user})

		req, err := http.NewRequest("POST", "/users/login", bytes.NewBufferString(body))
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("POST", "/users/login", handler.Login)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != 200 {
			t.Error("Expected response code to be 200, but is ", rr)
		}
	})

	t.Run("Given an email and password, when the login is sucessful, then return an access token in body", func(t *testing.T) {
		body := "{\"email\":\"hi@carlosrodrigo.me\",\"password\":\"carlos123\"}\n"
		handler := InitHandlerWithUsers(false, false, "HEREISTHETOKEN", map[string]model.User{user.Email: user})

		req, err := http.NewRequest("POST", "/users/login", bytes.NewBufferString(body))
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("POST", "/users/login", handler.Login)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != 200 {
			t.Error("Expected response code to be 200")
		}

		if rr.Body.String() != "{\"code\":200,\"message\":\"Successful Login!\",\"data\":{\"id\":\"123\",\"first_name\":\"\",\"last_name\":\"\",\"email\":\"hi@carlosrodrigo.me\",\"country\":\"\",\"phone\":\"\",\"activity\":{\"profession\":\"\",\"days\":null,\"hours\":null,\"avg_fee\":0,\"fee_currency\":\"\",\"session_duration\":0},\"payment_information\":{\"bank_account_id\":\"\",\"bank_account_alias\":\"\"},\"access_token\":\"HEREISTHETOKEN\"}}\n" {
			t.Errorf("Body is not ok, %q", rr.Body.String())
		}
	})

	t.Run("Given an incorrect body without email, when the login is performed, then an error should be returned", func(t *testing.T) {
		body := "{\"password\":\"password123\"}\n"
		handler := InitHandler(false, false, "")

		req, err := http.NewRequest("POST", "/users/login", bytes.NewBufferString(body))
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("POST", "/users/login", handler.Login)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusBadRequest {
			t.Error("Expected response code to be Bad Request")
		}

		if rr.Body.String() != "{\"code\":400,\"message\":\"Email is required\"}\n" {
			t.Errorf("Body is not ok, %q", rr.Body.String())
		}
	})

	t.Run("Given an incorrect body without password, when the login is performed, then an error should be returned", func(t *testing.T) {
		body := "{\"email\":\"hi@carlosrodrigo.me\"}\n"
		handler := InitHandler(false, false, "")

		req, err := http.NewRequest("POST", "/users/login", bytes.NewBufferString(body))
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("POST", "/users/login", handler.Login)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusBadRequest {
			t.Error("Expected response code to be Bad Request")
		}

		if rr.Body.String() != "{\"code\":400,\"message\":\"Password is required\"}\n" {
			t.Errorf("Body is not ok, %q", rr.Body.String())
		}
	})

	t.Run("Given an incorrect body, when the login is performed, then an error should be returned", func(t *testing.T) {
		body := "{\"bad_body\":\"password123\"\n"
		handler := InitHandler(false, false, "")

		req, err := http.NewRequest("POST", "/users/login", bytes.NewBufferString(body))
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("POST", "/users/login", handler.Login)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnprocessableEntity {
			t.Error("Expected response code to be Bad Request")
		}

		if rr.Body.String() != "{\"code\":422,\"message\":\"Incorrect Body\"}\n" {
			t.Errorf("Body is not ok, %q", rr.Body.String())
		}
	})
}

func TestUserRegistrationHandler(t *testing.T) {
	t.Run("Given a correct body, When we try to register a new user, Then status code should be 200", func(t *testing.T) {
		user := "{\"email\":\"hi@carlosrodrigo.me\",\"password\":\"carlos123\",\"confirmation_password\":\"carlos123\"}\n"
		handler := InitHandler(false, false, "")

		req1, err := http.NewRequest("POST", "/users/registration", bytes.NewBufferString(user))
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("POST", "/users/registration", handler.Register)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req1)

		if rr.Code != 200 {
			t.Error("Expected response code to be 200")
		}
	})

	t.Run("Given a body without email, When we try to register a new user, Then we should return a badrequest error with missing Email message", func(t *testing.T) {
		user := "{\"password\":\"carlos\",\"confirmation_password\":\"carlos\"}\n"
		handler := InitHandler(false, false, "")

		req1, err := http.NewRequest("POST", "/users/registration", bytes.NewBufferString(user))
		if err != nil {
			t.Fatal(err)
		}

		router := httprouter.New()
		router.Handle("POST", "/users/registration", handler.Register)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req1)

		if rr.Code != http.StatusBadRequest {
			t.Error("Expected response code to bad request")
		}

		if rr.Body.String() != "{\"code\":400,\"message\":\"Email is required\"}\n" {
			t.Errorf("Body is not ok, we have this %q", rr.Body.String())
		}
	})
}

func TestUserProfileHandler(t *testing.T) {
	t.Run("Given a save user profile request, When it hasn't an authorization token, Then should return an Authorization error", func(t *testing.T) {
		req, err := http.NewRequest("POST", "/users/db7c503c-f92b-11ea-90a1-c4b301c4553b/profile", nil)
		if err != nil {
			t.Fatal(err)
		}

		handle := InitHandler(false, false, "")
		router := httprouter.New()
		router.Handle("POST", "/users/:userID/profile", handle.SaveUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnauthorized {
			t.Errorf("Expected response code to be 401 - Unauthorized but is %v", rr.Code)
		}
	})

	t.Run("Given a jwt token in the request, When the id in the request is no the same that in the token, Then should return an error", func(t *testing.T) {
		req, err := http.NewRequest("POST", "/users/db7c503c-f92b-11ea-90a1-c4b301c4553b/profile", nil)
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.l0M_Y52vWA_blZ9OTWSUHdKIpoojkktnSM8ALihuBbU")
		if err != nil {
			t.Fatal(err)
		}

		handle := InitHandler(false, false, "")
		router := httprouter.New()
		router.Handle("POST", "/users/:userID/profile", handle.SaveUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnauthorized {
			t.Errorf("Expected response code to be %v but is %v, %q", http.StatusUnauthorized, rr.Code, rr.Body)
		}
	})
	t.Run("Given a save profile request, When the body is bad formed, Then it should return an error", func(t *testing.T) {
		body := `{"id":"db7c503c-f92b-11ea-90a1-c4b301c4553b","first_namexx"bank alias","wallet_account_id":"wallet id"}}`
		req, err := http.NewRequest("POST", "/users/db7c503c-f92b-11ea-90a1-123123153asda/profile", bytes.NewBufferString(body))
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.l0M_Y52vWA_blZ9OTWSUHdKIpoojkktnSM8ALihuBbU")
		if err != nil {
			t.Fatal(err)
		}

		handle := InitHandler(false, false, "")
		router := httprouter.New()
		router.Handle("POST", "/users/:userID/profile", handle.SaveUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusUnprocessableEntity {
			t.Errorf("Expected response code to be %v but is %v, %q", http.StatusUnprocessableEntity, rr.Code, rr.Body)
		}
	})
	t.Run("Given a save profile request, When the body user not exist, Then it should return an error", func(t *testing.T) {
		body := `{"id":"db7c503c-f92b-11ea-90a1-c4b301c4553b","first_name":"Carlos","last_name":"Rodrigo","country":"Argentina", "phone":"+541158051765","activity":{"profession":"Psychologist","days":["Monday", "Friday"],"hours":["8", "18"],"avg_fee":33.4,"fee_currency":"pesos","session_duration":45},"payment_information":{"bank_account_id":"bank id","bank_account_alias":"bank alias"}}`
		req, err := http.NewRequest("POST", "/users/db7c503c-f92b-11ea-90a1-123123153asda/profile", bytes.NewBufferString(body))
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.l0M_Y52vWA_blZ9OTWSUHdKIpoojkktnSM8ALihuBbU")
		if err != nil {
			t.Fatal(err)
		}

		handle := InitHandler(false, false, "")
		router := httprouter.New()
		router.Handle("POST", "/users/:userID/profile", handle.SaveUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusInternalServerError {
			t.Errorf("Expected response code to be %v but is %v, %q", http.StatusInternalServerError, rr.Code, rr.Body)
		}
	})
	t.Run("Given a save profile request, When the body is ok, Then it should return success", func(t *testing.T) {
		body := `{"id":"db7c503c-f92b-11ea-90a1-c4b301c4553b","first_name":"Carlos","last_name":"Rodrigo","country":"Argentina", "phone":"+541158051765","activity":{"profession":"Psychologist","days":["Monday", "Friday"],"hours":["8", "18"],"avg_fee":33.4,"fee_currency":"American Dollars","session_duration":45},"payment_information":{"bank_account_id":"bank id","bank_account_alias":"bank alias"}}`
		req, err := http.NewRequest("POST", "/users/db7c503c-f92b-11ea-90a1-123123153asda/profile", bytes.NewBufferString(body))
		req.Header.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJkYjdjNTAzYy1mOTJiLTExZWEtOTBhMS0xMjMxMjMxNTNhc2RhIiwiZXhwIjoyNjAwODE4MzA5fQ.l0M_Y52vWA_blZ9OTWSUHdKIpoojkktnSM8ALihuBbU")
		if err != nil {
			t.Fatal(err)
		}

		handle := InitHandlerWithUsers(false, false, "", map[string]model.User{
			"db7c503c-f92b-11ea-90a1-c4b301c4553b": model.User{
				ID: "db7c503c-f92b-11ea-90a1-c4b301c4553b",
			},
		})
		router := httprouter.New()
		router.Handle("POST", "/users/:userID/profile", handle.SaveUser)

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("Expected response code to be %v but is %v, %q", http.StatusOK, rr.Code, rr.Body)
		}
	})
}

type mockAppleSignInAuthError struct {
}

func (tm *mockAppleSignInAuthError) GetTokenInfo(token string) (auth.TokenInfo, error) {
	return auth.TokenInfo{}, errors.New("the token is not valid")
}

type mockAppleSignInAuth struct {
}

func (tm *mockAppleSignInAuth) GetTokenInfo(token string) (auth.TokenInfo, error) {
	return auth.TokenInfo{
		UniqueID: "testing_userId",
		Email:    "testing@testing.com",
	}, nil
}

func InitHandlerWithUsers(haveValidationError bool, haveTokenError bool, tokenToReturn string, users map[string]model.User) UsersHandler {
	var appleAuth auth.AppleSignInAuth

	if haveValidationError {
		appleAuth = &mockAppleSignInAuthError{}
	} else {
		appleAuth = &mockAppleSignInAuth{}
	}

	tokenGenerator := mocks.NewMockAuthTokenGenerator(haveTokenError, tokenToReturn)
	repository := stub.UsersRepository{
		ErrorInExist: false,
		ErrorInSave:  false,
		Users:        users,
	}
	service := services.NewUsersService(repository)
	return New(appleAuth, tokenGenerator, service, repository)
}
func InitHandler(haveValidationError bool, haveTokenError bool, tokenToReturn string) UsersHandler {
	return InitHandlerWithUsers(haveValidationError, haveTokenError, tokenToReturn, make(map[string]model.User))
}

func New(tm auth.AppleSignInAuth, tg auth.AuthTokenGenerator, service ports.UsersService, repository ports.UsersRepository) UsersHandler {
	appleSignInAction := actions.NewAppleSignInAction(repository)
	getUserAction := actions.NewGetUserAction(repository)
	loginAction := actions.NewLoginAction(service)
	registerAction := actions.NewRegisterUserAction(service)
	saveUserAction := actions.NewSaveUserAction(repository)

	return UsersHandler{
		AuthTokenGenerator: tg,
		AppleSignInAuth:    tm,
		AppleSignInAction:  appleSignInAction,
		GetUserAction:      getUserAction,
		LoginAction:        loginAction,
		RegisterAction:     registerAction,
		SaveUserAction:     saveUserAction,
	}
}
