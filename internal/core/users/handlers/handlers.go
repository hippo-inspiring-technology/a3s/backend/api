package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/actions"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/entities"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/domain/services"
	users "gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/core/users/infrastructure"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/auth"
	"gitlab.com/hippo-inspiring-technology/a3s/backend/api/internal/pkg/handler"
)

type UsersHandler struct {
	AuthTokenGenerator auth.AuthTokenGenerator
	AppleSignInAuth    auth.AppleSignInAuth
	AppleSignInAction  actions.AppleSignInAction
	GetUserAction      actions.GetAction
	LoginAction        actions.LoginAction
	RegisterAction     actions.RegisterAction
	SaveUserAction     actions.SaveAction
}

type AppleSignInRequestBody struct {
	AuthCode  string `json:"auth_code"`
	FirstName string `json:"given_name"`
	LastName  string `json:"family_name"`
}

func (h *UsersHandler) SignInWithApple(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	rb := AppleSignInRequestBody{}

	if r.Body == nil {
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}

	body, errReadBody := ioutil.ReadAll(r.Body)

	if errReadBody != nil {
		log.Println(errReadBody)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}

	errCloseReadBody := r.Body.Close()
	if errCloseReadBody != nil {
		log.Println(errCloseReadBody)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}

	errUnmarshalUser := json.Unmarshal(body, &rb)
	if errUnmarshalUser != nil {
		log.Println(errUnmarshalUser)
		handler.WriteResponseWithoutData(w, http.StatusUnprocessableEntity, "Incorrect Body")
		return
	}

	ti, errAuthCodeValidation := h.AppleSignInAuth.GetTokenInfo(rb.AuthCode)
	if errAuthCodeValidation != nil {
		log.Println(errAuthCodeValidation)
		handler.WriteResponseWithoutData(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	user, errAppleSignIn := h.AppleSignInAction.SignIn(ti.UniqueID, ti.Email, rb.FirstName, rb.LastName)
	if errAppleSignIn != nil {
		log.Println(errAppleSignIn)
		handler.WriteResponseWithoutData(w, http.StatusServiceUnavailable, "Oops, somtething bad happen")
		return
	}

	accessToken, errTokenGeneration := h.AuthTokenGenerator.GenerateNewToken(user.ID)
	if errTokenGeneration != nil {
		handler.WriteResponseWithoutData(w, http.StatusServiceUnavailable, "Oops, somtething bad happen")
		return
	}

	user.AccessToken = accessToken

	handler.WriteResponse(w, http.StatusOK, "Successful", user)
}

func (h *UsersHandler) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	_, errToken := auth.GetClaimsFromToken(r.Header.Get("Authorization"))
	if errToken != nil {
		log.Printf("Error - %q\n", errToken)
		handler.WriteResponseWithoutData(w, http.StatusUnauthorized, errToken.Error())
		return
	}

	userID := p.ByName("userID")
	user, errGetUser := h.GetUserAction.GetUser(userID)
	if errGetUser != nil {
		if errGetUser == entities.ErrUserNotRegistered {
			log.Printf("Error - %q\n", errGetUser)
			handler.WriteResponseWithoutData(w, http.StatusNotFound, entities.ErrUserNotRegistered.Error())
			return
		}
	}

	handler.WriteResponse(w, http.StatusOK, "Successful", user)
}

//Credentials represents the user credentials to access the domain
type Credentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (h *UsersHandler) Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	credentials := Credentials{}

	body, errReadBody := ioutil.ReadAll(r.Body)
	if errReadBody != nil {
		log.Println(errReadBody)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}
	errCloseReadBody := r.Body.Close()
	if errCloseReadBody != nil {
		log.Println(errCloseReadBody)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}
	errUnmarshalUser := json.Unmarshal(body, &credentials)
	if errUnmarshalUser != nil {
		log.Println(errUnmarshalUser)
		handler.WriteResponseWithoutData(w, http.StatusUnprocessableEntity, "Incorrect Body")
		return
	}

	loggedUserInfo, errLoginUser := h.LoginAction.Login(credentials.Email, credentials.Password)
	if errLoginUser != nil {
		log.Println(errLoginUser)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, errLoginUser.Error())
		return
	}

	accessToken, errTokenGeneration := h.AuthTokenGenerator.GenerateNewToken(loggedUserInfo.ID)
	if errTokenGeneration != nil {
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, errTokenGeneration.Error())
		return
	}

	loggedUserInfo.AccessToken = accessToken

	handler.WriteResponse(w, http.StatusOK, "Successful Login!", loggedUserInfo)
}

//User struct represents the information needed to register an user
type User struct {
	Email                string `json:"email"`
	Password             string `json:"password"`
	ConfirmationPassword string `json:"confirmation_password"`
}

func (h *UsersHandler) Register(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	user := User{}
	body, errReadBody := ioutil.ReadAll(r.Body)
	if errReadBody != nil {
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}

	errCloseReadBody := r.Body.Close()
	if errCloseReadBody != nil {
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}

	errUnmarshalUser := json.Unmarshal(body, &user)
	if errUnmarshalUser != nil {
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't unmarshal user json")
		return
	}

	registeredUser, errRegisterUser := h.RegisterAction.Register(user.Email, user.Password, user.ConfirmationPassword)
	if errRegisterUser != nil {
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, errRegisterUser.Error())
		return
	}

	accessToken, errTokenGeneration := h.AuthTokenGenerator.GenerateNewToken(registeredUser.ID)
	if errTokenGeneration != nil {
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, errTokenGeneration.Error())
		return
	}

	registeredUser.AccessToken = accessToken

	handler.WriteResponse(w, http.StatusOK, "User Registered", registeredUser)
}

func (h *UsersHandler) SaveUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	userProfile := entities.User{}
	claims, errToken := auth.GetClaimsFromToken(r.Header.Get("Authorization"))
	if errToken != nil {
		log.Printf("Error token - %q\n", errToken)
		handler.WriteResponseWithoutData(w, http.StatusUnauthorized, errToken.Error())
		return
	}

	userID := p.ByName("userID")
	if userID != claims.UserID {
		log.Printf("Error - %q - %q", userID, claims.UserID)
		handler.WriteResponseWithoutData(w, http.StatusUnauthorized, "Unauthorize action")
		return
	}

	body, errReadBody := ioutil.ReadAll(r.Body)
	if errReadBody != nil {
		log.Println(errReadBody)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}
	errCloseReadBody := r.Body.Close()
	if errCloseReadBody != nil {
		log.Println(errCloseReadBody)
		handler.WriteResponseWithoutData(w, http.StatusBadRequest, "Can't read body from request")
		return
	}
	errUnmarshalUser := json.Unmarshal(body, &userProfile)
	if errUnmarshalUser != nil {
		log.Println(errUnmarshalUser)
		handler.WriteResponseWithoutData(w, http.StatusUnprocessableEntity, "Incorrect Body")
		return
	}

	errSaveProfile := h.SaveUserAction.SaveUser(userProfile)
	if errSaveProfile != nil {
		log.Println(errSaveProfile)
		handler.WriteResponseWithoutData(w, http.StatusInternalServerError, "Can't save profile")
		return
	}

	handler.WriteResponseWithoutData(w, http.StatusOK, "Success")
}

func NewUsersHandler() UsersHandler {
	appleAuth := auth.NewAppleSignInAuth()
	authTokenGenerator := auth.NewAuthTokenGenerator()
	repository := users.NewUsersRepository()
	service := services.NewUsersService(repository)
	appleSignInAction := actions.NewAppleSignInAction(repository)
	getUserAction := actions.NewGetUserAction(repository)
	loginAction := actions.NewLoginAction(service)
	registerAction := actions.NewRegisterUserAction(service)
	saveUserAction := actions.NewSaveUserAction(repository)

	return UsersHandler{
		AuthTokenGenerator: authTokenGenerator,
		AppleSignInAuth:    appleAuth,
		AppleSignInAction:  appleSignInAction,
		GetUserAction:      getUserAction,
		LoginAction:        loginAction,
		RegisterAction:     registerAction,
		SaveUserAction:     saveUserAction,
	}
}
