package handler

import (
	"encoding/json"
	"net/http"
)

func WriteResponseWithoutData(w http.ResponseWriter, statusCode int, message string) {
	WriteResponse(w, statusCode, message, nil)
}
func WriteResponse(w http.ResponseWriter, statusCode int, message string, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	body := ResponseBody{
		Code:    statusCode,
		Message: message,
		Data:    data,
	}

	errEncode := json.NewEncoder(w).Encode(body)

	if errEncode != nil {
		panic(errEncode)
	}
}

//ResponseBody struct represents an standard body to include in a Response
type ResponseBody struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}
