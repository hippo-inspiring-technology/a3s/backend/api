package uuid

import (
	"errors"

	"github.com/google/uuid"
)

//New returns a new string UUID
func New() (string, error) {
	uuid, err := uuid.NewUUID()
	if err != nil {
		return "", errors.New("UUID can't be generated")
	}

	return uuid.String(), nil
}
