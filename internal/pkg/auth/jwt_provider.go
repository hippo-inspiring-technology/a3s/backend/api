package auth

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt"
)

var key = []byte("KEY")

//Claims struct represents the Claim part in a Json Web Token
type Claims struct {
	UserID string
	jwt.StandardClaims
}

type AuthTokenGenerator interface {
	GenerateNewToken(identifier string) (string, error)
}

type authTokenGenerator struct {
}

//GetNewToken provides a new Json Web Token for a identifier
func (ag *authTokenGenerator) GenerateNewToken(identifier string) (string, error) {
	expirationDate := time.Now().AddDate(0, 6, 0)
	claims := &Claims{
		UserID: identifier,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationDate.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString(key)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func NewAuthTokenGenerator() AuthTokenGenerator {
	return &authTokenGenerator{}
}

//GetClaimsFromToken extract the claims inside a token
func GetClaimsFromToken(tokenString string) (*Claims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	if err != nil {
		return &Claims{}, err
	}

	if claims, ok := token.Claims.(*Claims); ok && token.Valid {
		return claims, nil
	}

	return &Claims{}, errors.New("Invalid Token")
}
