package auth

import (
	"context"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"log"
	"os"
	"time"

	"github.com/Timothylock/go-signin-with-apple/apple"
	"github.com/golang-jwt/jwt/v4"
)

type AppleSignInAuth interface {
	GetTokenInfo(token string) (TokenInfo, error)
}

type appleSignInAuth struct {
}
type TokenInfo struct {
	UniqueID string
	Email    string
}

func (a appleSignInAuth) GetTokenInfo(token string) (TokenInfo, error) {
	teamID := os.Getenv("APPLE_TEAM_ID")
	clientID := os.Getenv("APPLE_CLIENT_ID")
	keyID := os.Getenv("APPLE_KEY_ID")
	secret := os.Getenv("APPLE_SECRET_KEY")

	secret, err := GenerateClientSecret(secret, teamID, clientID, keyID)
	if err != nil {
		return TokenInfo{}, err
	}
	// Generate a new validation client
	client := apple.New()

	vReq := apple.AppValidationTokenRequest{
		ClientID:     clientID,
		ClientSecret: secret,
		Code:         token,
	}

	var resp apple.ValidationResponse

	// Do the verification
	err = client.VerifyAppToken(context.Background(), vReq, &resp)
	if err != nil {
		return TokenInfo{}, err
	}

	if resp.Error != "" {
		log.Println(resp)
		return TokenInfo{}, errors.New("Error in token validation")
	}

	// Get the unique user ID
	unique, err := apple.GetUniqueID(resp.IDToken)
	if err != nil {
		return TokenInfo{}, err
	}

	// Get the email
	claim, err := apple.GetClaims(resp.IDToken)
	if err != nil {
		return TokenInfo{}, err
	}

	email := (*claim)["email"].(string)

	return TokenInfo{
		UniqueID: unique,
		Email:    email,
	}, nil
}

func NewAppleSignInAuth() AppleSignInAuth {
	return &appleSignInAuth{}
}

func GenerateClientSecret(signingKey, teamID, clientID, keyID string) (string, error) {
	block, _ := pem.Decode([]byte(signingKey))
	if block == nil {
		return "", errors.New("empty block after decoding")
	}

	privKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return "", err
	}

	// Create the Claims
	now := time.Now()
	claims := &jwt.StandardClaims{
		Issuer:    teamID,
		IssuedAt:  now.Unix(),
		ExpiresAt: now.Add(time.Hour*24*180 - time.Second).Unix(), // 180 days
		Audience:  "https://appleid.apple.com",
		Subject:   clientID,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	token.Header["alg"] = "ES256"
	token.Header["kid"] = keyID

	return token.SignedString(privKey)
}
