docker build . -t hippo-api-v1

docker stop hippo-api || true && docker rm hippo-api || true

docker run -d --name hippo-api -p 3000:3000 --env-file dev.env hippo-api-v1
